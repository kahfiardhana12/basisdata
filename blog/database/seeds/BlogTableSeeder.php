<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class BlogTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('blog')->insert([
            'title' => '7 Makanan Tradisional Khas Jawa yang Cocok Dijadikan saat HUT RI Ke-75',
            'content' => 'Jakarta - Makanan tradisional khas Jawa pilihannya sangat beragam. Semua makanan ini memiliki makna spesial sehingga jadi sajian popular saat HUT RI yang ke-75 ini.',
            'category' => 'HUT RI Ke-75'
        ]);
    }
}
